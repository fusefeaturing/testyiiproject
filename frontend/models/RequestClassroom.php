<?php


namespace frontend\models;
use Yii;

/**
 * This is the model class for table "request_classroom".
 *
 * @property int $rq_classroom_id
 * @property int $member_id
 * @property int $classroom_id
 * @property string $rq_day_start
 * @property string $rq_day_end
 * @property string $rq_time_start
 * @property string $rq_time_end
 * @property string $rq_date_now
 * @property int $rq_approve_status
 *
 * @property Approve[] $approves
 * @property Classroom $classroom
 * @property Member $member
 */
class RequestClassroom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request_classroom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'classroom_id', 'rq_day_start', 'rq_day_end', 'rq_time_start', 'rq_time_end', 'rq_approve_status'], 'required'],
            [['member_id', 'classroom_id', 'rq_approve_status'], 'integer'],
            [['rq_day_start', 'rq_day_end', 'rq_time_start', 'rq_time_end', 'rq_date_now'], 'safe'],
            [['classroom_id'], 'exist', 'skipOnError' => true, 'targetClass' => Classroom::className(), 'targetAttribute' => ['classroom_id' => 'classroom_id']],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => Member::className(), 'targetAttribute' => ['member_id' => 'member_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rq_classroom_id' => 'Rq Classroom ID',
            'member_id' => 'Member ID',
            'classroom_id' => 'Classroom ID',
            'rq_day_start' => 'Rq Day Start',
            'rq_day_end' => 'Rq Day End',
            'rq_time_start' => 'Rq Time Start',
            'rq_time_end' => 'Rq Time End',
            'rq_date_now' => 'Rq Date Now',
            'rq_approve_status' => 'Rq Approve Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApproves()
    {
        return $this->hasMany(Approve::className(), ['rq_classroom_id' => 'rq_classroom_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassroom()
    {
        return $this->hasOne(Classroom::className(), ['classroom_id' => 'classroom_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::className(), ['member_id' => 'member_id']);
    }
}
