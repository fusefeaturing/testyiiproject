<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "classroom".
 *
 * @property int $classroom_id
 * @property int $building_id
 * @property int $equipment_id
 * @property string $equipment_name
 * @property int $furniture_t_id
 * @property int $furniture_c_id
 * @property string $classroom_name
 * @property int $classroom_num_seat
 *
 * @property FurnitureC $furnitureC
 * @property FurnitureT $furnitureT
 * @property Equipment $equipment
 * @property Building $building
 * @property RequestClassroom[] $requestClassrooms
 */
class Classroom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'classroom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id', 'equipment_id', 'furniture_t_id', 'furniture_c_id', 'classroom_num_seat'], 'integer'],
            [['classroom_name'], 'required'],
            [['equipment_name'], 'string', 'max' => 255],
            [['classroom_name'], 'string', 'max' => 50],
            [['furniture_c_id'], 'exist', 'skipOnError' => true, 'targetClass' => FurnitureC::className(), 'targetAttribute' => ['furniture_c_id' => 'furniture_c_id']],
            [['furniture_t_id'], 'exist', 'skipOnError' => true, 'targetClass' => FurnitureT::className(), 'targetAttribute' => ['furniture_t_id' => 'furniture_t_id']],
            [['equipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Equipment::className(), 'targetAttribute' => ['equipment_id' => 'equipment_id']],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::className(), 'targetAttribute' => ['building_id' => 'building_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'classroom_id' => 'Classroom ID',
            'building_id' => 'Building ID',
            'equipment_id' => 'Equipment ID',
            'equipment_name' => 'Equipment Name',
            'furniture_t_id' => 'Furniture T ID',
            'furniture_c_id' => 'Furniture C ID',
            'classroom_name' => 'Classroom Name',
            'classroom_num_seat' => 'Classroom Num Seat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFurnitureC()
    {
        return $this->hasOne(FurnitureC::className(), ['furniture_c_id' => 'furniture_c_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFurnitureT()
    {
        return $this->hasOne(FurnitureT::className(), ['furniture_t_id' => 'furniture_t_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipment()
    {
        return $this->hasOne(Equipment::className(), ['equipment_id' => 'equipment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::className(), ['building_id' => 'building_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestClassrooms()
    {
        return $this->hasMany(RequestClassroom::className(), ['classroom_id' => 'classroom_id']);
    }
}
