<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property int $member_id
 * @property int $faculty_id
 * @property int $branch_id
 * @property string $member_username
 * @property string $member_password
 * @property string $member_profile_pic
 * @property string $member_firstname
 * @property string $member_lastname
 * @property string $member_tel
 * @property string $member_type
 *
 * @property RequestClassroom[] $requestClassrooms
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['faculty_id', 'branch_id'], 'integer'],
            [['member_username', 'member_password', 'member_firstname', 'member_lastname', 'member_tel', 'member_type'], 'required'],
            [['member_username', 'member_tel'], 'string', 'max' => 15],
            [['member_password'], 'string', 'max' => 50],
            [['member_profile_pic'], 'string', 'max' => 255],
            [['member_firstname', 'member_lastname'], 'string', 'max' => 100],
            [['member_type'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'member_id' => 'ไอดี',
            'faculty_id' => 'คณะ',
            'branch_id' => 'สาขา',
            'member_username' => 'รหัสผู้ใช้งาน',
            'member_password' => 'รหัสผ่าน',
            'member_profile_pic' => 'รูปโปรไฟล์',
            'member_firstname' => 'ชื่อ',
            'member_lastname' => 'นามสกุล',
            'member_tel' => 'เบอร์โทรศัพท์',
            'member_type' => 'ประเภทผู้ใช้งาน',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestClassrooms()
    {
        return $this->hasMany(RequestClassroom::className(), ['member_id' => 'member_id']);
    }
}
