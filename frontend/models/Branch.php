<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "branch".
 *
 * @property int $branch_id
 * @property int $faculty_id
 * @property string $branch_name
 */
class Branch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'branch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['faculty_id', 'branch_name'], 'required'],
            [['faculty_id'], 'integer'],
            [['branch_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'branch_id' => 'Branch ID',
            'faculty_id' => 'Faculty ID',
            'branch_name' => 'Branch Name',
        ];
    }
}
