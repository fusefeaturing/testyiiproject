<?php

namespace frontend\controllers;
use yii\web\Controller;
use frontend\models\RequestClassroom;


class RqclassroomController extends Controller {
    public function actionRequest() {
        $req = RequestClassroom :: find()->all();
        return $this->render('request',['req' => $req]);
    }
}