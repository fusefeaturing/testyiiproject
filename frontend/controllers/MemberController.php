<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Member;

class MemberController extends Controller {
    public function actionMember() {
        $mem = Member::find()->all();
        return $this->render('member',['mem' => $mem]);
    }
    public function actionMembercreate(){
        $model = new Member;
        if(Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                return $this->redirect(['member']);
            }
        }
        
        return $this->render('membercreate' ,['model' => $model]);
    }
    public function actionMemberupdate($id) {
        $model = Member :: findOne($id);
        if(Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                return $this->redirect(['member']);
            }
        }
        return $this->render('memberupdate',['model' => $model]);
    }
    public function actionMemberdelete($id) {
        $model = Member :: findOne($id);

        if($model->delete()) {
            return $this->redirect(['member']);
        } else{
            echo 'fail...';
        }
    }
}
