<table class="table">
    <thead style="background-color: #5cb85c;">
        <tr>
            <th>ไอดี</th>
            <th>ชื่อผู้ใช้งาน</th>
            <th>ห้องเรียน</th>
            <th>วันที่เริ่มต้น</th>
            <th>วันที่สิ้นสุด</th>
            <th>เวลาเริ่มต้น</th>
            <th>เวลาสิ้นสุด</th>
            <th>เวลาที่ขอใช้</th>
            <th>สถานะ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($req as $key => $req) : ?>
        <tr>
            <td><?= $req->rq_classroom_id; ?></td>
            <td><?= $req->member_id; ?></td>
            <td><?= $req->classroom_id; ?></td>
            <td><?= $req->rq_day_start; ?></td>
            <td><?= $req->rq_day_end; ?></td>
            <td><?= $req->rq_time_start; ?></td>
            <td><?= $req->rq_time_end; ?></td>
            <td><?= $req->rq_date_now; ?></td>
            <td><?= $req->rq_approve_status; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
