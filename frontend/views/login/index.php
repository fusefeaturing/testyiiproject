<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = "Login";
?>

<?php ActiveForm::begin() ?>
<h3><?= $this->title ?></h3>
<div class="form-group">
    <?= Html::label("username : ") ?>
    <?= Html::TextInput("username", "", ['required'=>'required']) ?>
</div>
<div class="form-group">
    <?= Html::label("password : ") ?>
    <?= Html::passwordInput("password", "", ['required'=>'required']) ?>
</div>
<div class="form-group">
  
    <?= Html::submitButton("Login", []) ?>
</div>

<?php ActiveForm::end() ?>