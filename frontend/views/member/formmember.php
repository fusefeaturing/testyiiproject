<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="well">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'faculty_id')->textInput(); ?>
    <?= $form->field($model, 'branch_id')->textInput(); ?>
    <?= $form->field($model, 'member_username')->textInput(); ?>
    <?= $form->field($model, 'member_password')->textInput(); ?>
    <?= $form->field($model, 'member_profile_pic')->textInput(); ?>
    <?= $form->field($model, 'member_firstname')->textInput(); ?>
    <?= $form->field($model, 'member_lastname')->textInput(); ?>
    <?= $form->field($model, 'member_tel')->textInput(); ?>
    <?= $form->field($model, 'member_type')->textInput(); ?>
    
    <?=Html :: submitButton('คกลง',['class' => 'btn btn-success'])?>
    <?php $form = ActiveForm::end(); ?>
</div>