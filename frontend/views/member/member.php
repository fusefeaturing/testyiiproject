<?php
use yii\helpers\Url;
use yii\web\View;
?>
<a href="<?= Url::to(['member/membercreate']); ?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i>เพิ่มข้อมูล</a>
<div style="height:10px;"></div>

<table class="table">
    <thead style="background-color: #5cb85c;">
        <tr>
            <th>ไอดี</th>
            <th>คณะ</th>
            <th>สาขา</th>
            <th>รหัสผู้ใช้งาน</th>
            <th>รหัสผ่าน</th>
            <th>รูปโปรไฟล์</th>
            <th>ชื่อ</th>
            <th>นามสกุล</th>
            <th>เบอร์โทรศัพท์</th>
            <th>ประเภทผู้ใช้งาน</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($mem as $key => $memm) : ?>
        <tr>
            <td><?= $memm->member_id; ?></td>
            <td><?= $memm->faculty_id; ?></td>
            <td><?= $memm->branch_id; ?></td>
            <td><?= $memm->member_username; ?></td>
            <td><?= $memm->member_password; ?></td>
            <td><?= $memm->member_profile_pic; ?></td>
            <td><?= $memm->member_firstname; ?></td>
            <td><?= $memm->member_lastname; ?></td>
            <td><?= $memm->member_tel; ?></td>
            <td><?= $memm->member_type; ?></td>
            <td>
                <a href="<?= Url::to(['member/memberupdate', 'id'=> $memm->member_id]); ?>"><i class="glyphicon glyphicon-pencil"></i></a> | 
                <a href="<?= Url::to(['member/memberdelete', 'id'=> $memm->member_id]); ?>" class="delete"><i class="glyphicon glyphicon-trash"></i></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php

$this->registerJS("
    $('.delete').click(function() { 
        if(!confirm('Please confirm')){
            return false;
        }
    });
");
?>